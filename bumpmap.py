# -*- coding: utf-8 -*-
#
# License: GNU GPL v3 -- See 'LICENCE' file.
#   

import numpy as np
import xarray as xr


def mask_normalmap(maskArray, bumpMapTile):
    (r, c) = maskArray.shape
    bm = _tile_to_extent(bumpMapTile, maskArray.shape)
    bm = bm[:r,:c]  # ensures that bm is the right shape
    returnNormalMap = np.zeros((r, c, 3))
    returnNormalMap[:,:,2] = 1.0
    n = np.broadcast_to(maskArray, (3, r, c))
    n = np.moveaxis(n, 0, 2)
    returnNormalMap[n] = bm[n]
    return returnNormalMap


def _tile_to_extent(bumpMapTile, shp):
    (r, c) = shp
    r_tile_count = (r // bumpMapTile.shape[0]) + 2
    c_tile_count = (c // bumpMapTile.shape[1]) + 2
    img = np.tile(bumpMapTile, (r_tile_count, c_tile_count, 1))
    center_r = img.shape[0] // 2
    center_c = img.shape[1] // 2
    returnImage = img[center_r - (r//2):center_r + (r//2) + 1, center_c - (c//2):center_c+(c//2) + 1, 0:3]
    return _normalize_bumpmap_vectors(returnImage)


def _normalize_bumpmap_vectors(bumpMap):
    """
    Unfortunately, the tiff image can be ints of different size (8-bit, 16-bit), or floats.  We need to 
    normalize these such that the values range from -1 to 1, just as surface normal vectors should do. 
    """
    if bumpMap.dtype == 'uint8':
        return ((bumpMap / (2 ** 8 - 1)) * 2) - 1
    if bumpMap.dtype == 'uint16':
        return ((bumpMap / (2 ** 16 - 1)) * 2) - 1
    if bumpMap.dtype == 'float32':
        return (bumpMap - 0.5) * 2
    if bumpMap.dtype == 'float64':
        return (bumpMap - 0.5) * 2
    return bumpMap


def normalmap(S, B):
    """
    :param S: surface normal vector array (row, col, band) -- this should be an xarray with dimensions Nx, Ny, Nz
    :param B: bump-map as 3-band surface normal vectors (row, col, band) - Also should be an xarray with same dimensions
    :return: bumpified surface normal vectors as an xarray DataArray... dimensioned (row, col, component) with 
              components Nx, Ny, Nz.  This is intended to look like a standard SNV dataset for the extent, but the
              vectors have been adjusted from S by dotting with B.
    """
    (r, c, _) = S.data.shape

    Uz = S.sel(component="Nx") / S.sel(component="Nz")
    mag_u = np.sqrt(1 + Uz**2)

    Vz = S.sel(component="Ny") / S.sel(component="Nz")
    mag_v = np.sqrt(1 + Vz**2)

    basis=xr.Dataset(
        data_vars=dict(
            U=(["rows", "cols", "component"], np.stack([1/mag_u, np.zeros((r,c)), -Uz/mag_u], 2)),
            V=([ "rows", "cols", "component"], np.stack([np.zeros((r,c)), 1/mag_v, -Vz/mag_v], 2)), 
            S=(["rows", "cols", "component"], S.data)
            ),
        coords=dict(
            component=["Nx", "Ny", "Nz"]
            ),
    )
    ## I'm doing this longhand -- but I think it would be better if we could trust np.matmul to do it
    ## https://numpy.org/doc/stable/reference/generated/numpy.matmul.html
    Nx = basis['U'].sel(component="Nx") * B.sel(component="Nx")  + \
        basis['V'].sel(component="Nx") * B.sel(component="Ny") + \
        basis['S'].sel(component="Nx") * B.sel(component="Nz")

    Ny = basis['U'].sel(component="Ny") * B.sel(component="Nx")  + \
        basis['V'].sel(component="Ny") * B.sel(component="Ny") + \
        basis['S'].sel(component="Ny") * B.sel(component="Nz")
    
    Nz = basis['U'].sel(component="Nz") * B.sel(component="Nx")  + \
        basis['V'].sel(component="Nz") * B.sel(component="Ny") + \
        basis['S'].sel(component="Nz") * B.sel(component="Nz")
    
    mag = np.sqrt(Nx**2 + Ny**2 + Nz**2) # normalize so that vectors are guaranteed of length one.

    # Return a dataarray... which is how this product will be consumed... dataarrays in, dataarray out.
    return xr.DataArray(data=    np.stack([Nx / mag, Ny / mag, Nz / mag], 2),
        dims=["rows", "cols", "component"],
        coords=dict(component=["Nx", "Ny", "Nz"])
    )

