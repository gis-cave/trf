# -*- coding: utf-8 -*-
#
# License: GNU GPL v3 -- See 'LICENCE' file.
#
#
import numpy as np
import xarray as xr
import logging
from math import sqrt, pi, floor
from scipy.ndimage import convolve as skconvolve
_logger = logging.getLogger(__name__)


def study(dem, c):
    """
    :param dem: the numpy array of height values
    :param c: cell height and width in same units as the elevations/heights encoded in the dem array.
        Note that we are assuming projected coordinate system where cells are square --> same width and height. 
    :return: An xarray Dataset with two data arrays:  "DEM" the input dem, and "SNV" the surface normal vectors derived from gradients in x and y directions.
    """
    _logger.debug("Computing Surface Normal Vectors...")
    snv = surface_normals(dem, c)
    _terrain=xr.Dataset(
        data_vars=dict(
            DEM=(["rows", "cols"], dem.astype(np.float64)),
            SNV=([ "rows", "cols", "component"], snv)
            ),
        coords=dict(
            component=["Nx", "Ny", "Nz"]
            ),
        attrs=dict(cellwidth=c)
    )
    _logger.debug("Studied DEM: {} rows by {} cols / cellsize is {}".format(dem.shape[0], dem.shape[1], c))
    return _terrain


def surface_normals(dem, c):
    # Surface normal vectors arise directly from the gradients in x and y axial directions. 
    [dx, dy] = _numpy_gradient(dem, c) 
    mag = np.sqrt(dx**2 + dy**2 + 1)
    _logger.debug(f"Surface Normal Vectors for {mag.shape[0]}x{mag.shape[1]} extent.")
    return np.stack([-dx / mag, -dy / mag, 1 / mag], 2)


def slope(t, output="Degrees"):
    """
    : param t: xarray Dataset
    : return: a numpy array of slope values (in degrees) the same shape as the input terrain data.

    If you want slope to be in the terrain dataset as a separate dataarray, call this function like so: 
      > terrain = surface.study(dem_array, 7.5)  # numpy array and a cellwidth
      > terrain['slope'] = xr.DataArray(data=surface.slope(terrain), dims=["rows", "cols"])
      > terrain.info
        <bound method Dataset.info of <xarray.Dataset>
        Dimensions:    (rows: 267, cols: 267, component: 3)
        Coordinates:
        * component  (component) <U2 'Nx' 'Ny' 'Nz'
        Dimensions without coordinates: rows, cols
        Data variables:
            DEM        (rows, cols) float32 0.0 0.0 0.0 0.0 0.0 ... 0.0 0.0 0.0 0.0 0.0
            SNV        (rows, cols, component) float32 -0.0 -0.0 1.0 ... -0.0 -0.0 1.0
            slope      (rows, cols) float32 0.0 0.0 0.0 0.0 0.0 ... 0.0 0.0 0.0 0.0 0.0
        Attributes:
            cellwidth:  7.5>
    
      You can then access slope as terrain['slope'] later. 
    """
    if output not in ["Radians", "Normalized"]:
        output = "Degrees"

    try:
        z = t['SNV'].sel(component="Nz").data
    except KeyError:
        _logger.exception("It is pitch black. You are likely to be eaten by a grue.\n" ,
            "The terrain does not have a Surface Normal Vector (SNV) layer.")
        return None

    _s = np.arccos(z)  # slope is zero for flat/planar areas; pi/2 for vertical areas.

    if output == "Radians":
        return _s
    if output == "Degrees":
        return _s * (180/pi)
    if output == "Normalized":
        return np.clip( 1.0 - (_s / (np.pi/2)), 0.0,  1.0 )
    # We should never get here...
    return None


def aspect(t):
    """
    : param t: xarray Dataset
    : return: a numpy array of aspect values the same shape as the input terrain data.

    If you want aspect to be in the terrain dataset as a separate dataarray, call with:
        > terrain = surface.study(dem_array, 7.5)  # numpy array and a cellwidth
        > terrain['aspect'] = xr.DataArray(data=surface.aspect(terrain), dims=["rows", "cols"])
        > terrain.info
        <bound method Dataset.info of <xarray.Dataset>
        Dimensions:    (rows: 267, cols: 267, component: 3)
        Coordinates:
        * component  (component) <U2 'Nx' 'Ny' 'Nz'
        Dimensions without coordinates: rows, cols
        Data variables:
            DEM        (rows, cols) float32 0.0 0.0 0.0 0.0 0.0 ... 0.0 0.0 0.0 0.0 0.0
            SNV        (rows, cols, component) float32 -0.0 -0.0 1.0 ... -0.0 -0.0 1.0
            aspect     (rows, cols) float32 0.0 0.0 0.0 0.0 0.0 ... 0.0 0.0 0.0 0.0 0.0
        Attributes:
            cellwidth:  7.5>
        
        You can then access aspect values as terrain['aspect'].
    """

    try:
        x = t['SNV'].sel(component="Nx").data
        y = t['SNV'].sel(component="Ny").data
        z = t['SNV'].sel(component="Nz").data
    except KeyError:
        _logger.exception("It is pitch black. You are likely to be eaten by a grue.\n" ,
            "The terrain does not have a Surface Normal Vector (SNV) layer.")
        return None

    slope_deg =  np.arccos(z) * (180/pi)
    aspect_tmp = ((180/pi) * np.arctan2(y, -x)) + 180  # This is clockwise from E
    # convert to compass directions (clockwise from north=0)
    aspect =( 360 + 90 + aspect_tmp) % 360
    aspect[slope_deg==0] = -1 
    return aspect


def _numpy_gradient(A, cellwidth, level=1, method="N82"):
    """
    :param A: The array of heights. For terrains, this is the DEM.
    :param cellwidth: cell height and width.  This should be in same units as the heights encoded in A
    :param kwargs:
        (optional) method= : Which method to estimate gradient? A list of valid method identifiers is below.
        (optiona) level= : how much to generalize by increasing the sample distance. level is an integer which
        indicates the number of cells in each direction to go for samples.  level=1 is standard (kings case).
        level=2 samples cells in the orthogonal and diagonal directions, but 2 cells away.
    :return: A two-element array holding rasters for nabla X and nabla Y for the given height field.
    """
    # Level 1 is a 3x3 kernel
    # Level 2 is a 5x5 kernel
    # Level 3 is a 7x7 kernel
    # Level X is a square kernel of 2*X + 1 pixels.
    k=max(1,level) # sanity check -- no negative or zero values. 

    A = np.pad(A, k, 'edge')  # adds a border; covers edge cases.  Will be trimmed in the slicing manipulations.
    # Create slices of the height array, shifted in each direction.
    z1 = A[0:-(2*k), 0:-(2*k)]
    z2 = A[0:-(2*k), k:-k]
    z3 = A[0:-(2*k), (2*k):]

    z4 = A[k:-k, 0:-(2*k)]
    z5 = A[k:-k, k:-k]
    z6 = A[k:-k, (2*k):]

    z7 = A[(2*k):, 0:-(2*k)]
    z8 = A[(2*k):, k:-k]
    z9 = A[(2*k):, (2*k):]

    #
    # +-------+-------+-------+
    # |       |       |       |
    # |  z1   |   z2  |   z3  |
    # |       |       |       |
    # +-------+-------+-------+
    # |       |       |       |
    # |   z4  |   z5  |   z6  |
    # |       |       |       |
    # +-------+-------+-------+
    # |       |       |       |
    # |   z7  |   z8  |   z9  |
    # |       |       |       |
    # +-------+-------+-------+
    #
    # For a given cell at 'z5', we can now compute differences among its neighbors by referring to the arrays
    # at z1 toz9 .  Subtracting z6 from z4, for example, yields an array where the cell values are the difference
    # between the west and east orthogonal cells for all cells in the array.

    # I'm using these different views into the array rather than convolutions, because it is marginally faster
    # than importing ndimage and using ndimage.convolve with the relevant kernels.  Slicing in this way also
    # avoids making copies of the DEM array.

    mthd = method.upper()
    # just in case we get handed a bogus method code:
    if mthd not in ['FFD', 'SIMPLED', 'N4', '2FD', 'N82', '3FDWRSD', 'N8R', '3FDWRD', 'N8E', '3FD']:
        mthd = 'N82'

    # Various methods to compute the partial derivatives for change in z with respect to x and y
    # are available.  dz/dx is dz_dx and dz/dy is dz_dy

    # N4 - Four Neighbors.  This is a second order, finite difference (i.e. the Rook's Case).
    #      TAPES-G calls this "2FD".
    #      Fleming and Hoffer, 1979; Ritter, 1987; Zevenbergen and Thorne (1987); O'Neill & Mark (1987)
    #
    # This is an easy, intuitive case.
    if (mthd == "N4" or mthd == "2FD"):
        dz_dx = (z6 - z4) / (2 * cellwidth * k)
        dz_dy = (z2 - z8) / (2 * cellwidth * k)

    # N82 - 8 neighbors, weighted. Third order finite difference weighted by reciprocal of squared distance
    #      differential weights (Horn, 1981); Sobel operator (Richards, 1986).
    # TAPES-G name is 3FDWRSD
    # This is the method Esri uses for computing gradients (and from gradients, slope and aspect).
    if (mthd == "N82" or mthd == "3FDWRSD"):
        dz_dx = ((z3 + 2 * z6 + z9) - (z1 + 2 * z4 + z7)) / (8 * cellwidth * k)
        dz_dy = ((z1 + 2 * z2 + z3) - (z7 + 2 * z8 + z9)) / (8 * cellwidth * k)
    # This is essentially a Sobel edge-detection filter. 


    # SimpleD - Simple Difference
    if (mthd == "SIMPLED"):
        dz_dx = (z5 - z8) / (cellwidth * k)
        dz_dy = (z5 - z4) / (cellwidth * k)

    #
    # FFD - Finite Frame Difference
    if (mthd == "FFD"):
        dz_dx = (z1 - z7 + z3 - z9) / (4 * cellwidth * k)
        dz_dy = (z9 - z7 + z3 - z1) / (4 * cellwidth * k)

    # N8R - 8 neighbors, weighted. Third order finite difference weighted by reciprocal  distance.
    #      Unwin, 1981 -  3FDWRD
    #
    if (mthd == "N8R" or mthd == "3FDWRD"):
        r = sqrt(2)
        dz_dx = ((z3 + r * z6 + z9) - (z1 + r * z4 + z7)) / ((4 + 2 * r) * cellwidth * k)
        dz_dy = ((z1 + r * z2 + z3) - (z7 + r * z8 + z9)) / ((4 + 2 * r) * cellwidth * k)

    # N8E - 8 neighbors, even weighting. "Queen's case": Equivalent to fitting a second order trend surface.
    #      Identical weights. TAPES-G calls this "3FD" -- "Third order finite difference"
    # 	    Horn, 1981; Heerdegen and Beran, 1982;  Wood, 1996
    if (mthd == "N8E" or mthd == "3FD"):
        dz_dx = ((z3 + z6 + z9) - (z1 + z4 + z7)) / (6 * cellwidth * k)
        dz_dy = ((z1 + z2 + z3) - (z7 + z8 + z9)) / (6 * cellwidth * k)

    return [dz_dx, dz_dy]


def profile_curvature(t, span=1):
    """
    :param t: A terrain with a 'DEM' layer.
    :return: profile curvature
    """
    k=max(1,span) # sanity check -- no negative or zero values. 
    
    A = np.pad(t['DEM'].data, k, 'edge')  # adds a border; covers edge cases.  Will be trimmed in the slicing manipulations.
    # Create slices of the height array, shifted in each direction.
    z1 = A[0:-(2*k), 0:-(2*k)]
    z2 = A[0:-(2*k), k:-k]
    z3 = A[0:-(2*k), (2*k):]

    z4 = A[k:-k, 0:-(2*k)]
    z5 = A[k:-k, k:-k]
    z6 = A[k:-k, (2*k):]

    z7 = A[(2*k):, 0:-(2*k)]
    z8 = A[(2*k):, k:-k]
    z9 = A[(2*k):, (2*k):]

    cellwidth = t.attrs['cellwidth'] * k
    # from Zevenbergen and Thorne
    D = ((z4+z6)/2  - z5 )/ cellwidth**2
    E = ((z2+z8)/2  - z5 )/ cellwidth**2
    F = (z3 - z1 + z7 - z9) / 4 * cellwidth**2
    G = (z6 - z4) / 2 * cellwidth
    H = (z8 - z2) / 2 * cellwidth

    old_settings = np.seterr(divide='ignore') # To suppress warnings about dividing by zero... 
    # In caseswhere both Nabla X and Nabla Y are zero, the following will div by zero.
    _kurvature = (2 * (D * G**2 + E * H**2 + F*G*H) ) / (G**2 + H**2)
    _kurvature[np.isnan(_kurvature)] = 0
    np.seterr(**old_settings)  # reset to default
    return _kurvature
