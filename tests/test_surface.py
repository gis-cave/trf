#
#

import numpy as np
from numpy.testing import assert_allclose

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from trf import surface

testDEM = np.array([
        [10, 10,  10, 10, 10],
        [10, 10,  10, 10, 10],
        [10, 10,  20, 10, 10],
        [10, 10,  10, 10, 10],
        [10, 10,  10, 10, 10]
    ])

def test_gradient_DefaultMethod():
    """
    Does the default/unspecified method produce result identical to explicitly calling 'N82'? 
    """
    dx_1, dy_1 = surface._numpy_gradient(testDEM, 10)
    dx_2, dy_2 = surface._numpy_gradient(testDEM, 10, method='N82')
    assert_allclose(dx_1, dx_2, atol=1e-4)
    assert_allclose(dy_1, dy_2, atol=1e-4)


def test_gradient_DEMplus10():
    """
    If elevation increases by a uniform amount, gradient should be unchanged. 
    Raising the DEM by 10 meters for all cells should not affect slope -- a measure of relative change from one cell to the next. 
    """
    dx_1, dy_1 = surface._numpy_gradient(testDEM, 10)
    dx_2, dy_2 = surface._numpy_gradient(testDEM+10, 10)
    assert_allclose(dx_1, dx_2, atol=1e-4)
    assert_allclose(dy_1, dy_2, atol=1e-4)


def test_gradient_Cellwidth():
    """
    Half the cell width should double the slope.
    """
    dx_1, dy_1 = surface._numpy_gradient(testDEM, 10)
    dx_2, dy_2 = surface._numpy_gradient(testDEM, 5)
    assert_allclose(2 * dx_1, dx_2, atol=1e-4)
    assert_allclose(2 * dy_1, dy_2, atol=1e-4)


def test_constant_slope_x():
    """
   For a test array that slopes only in y, does it produce gradient=0 in x?
    """
    testDEM = np.array([
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50]
    ])
    _, dy = surface._numpy_gradient(testDEM, 10)
    assert_allclose(dy, np.broadcast_to(0, testDEM.shape))


def test_constant_slope_y():
    """
    For a test array that slopes only in x, does it produce gradient=0 in y?
    """
    testDEM = np.array([
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50]
    ])
    testDEM = np.rot90(testDEM)
    dx, _ = surface._numpy_gradient(testDEM, 10)
    assert_allclose(dx, np.broadcast_to(0, testDEM.shape))


def test_normalized_normals():
    """
    does surface.normalze() return x, y, and z components that yield a vector of unit length one?
    """
    t = surface.study(testDEM, 10)
    x = t['SNV'].sel(component="Nx")
    y = t['SNV'].sel(component="Ny")
    z = t['SNV'].sel(component="Nz")
    magnitude = np.sqrt(x**2 + y**2 + z**2)
    assert_allclose(magnitude, np.broadcast_to(1, testDEM.shape))

def test_normals_slope_left_right():
    """
    A slope facing west with 45 degree slope should produce a uniform vector field where x and z components are equal and y is zero
    """
    testDEM = np.array([
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50]
    ])
    t = surface.study(testDEM, 10)
    assert_allclose(t['SNV'].sel(component="Ny"), np.broadcast_to(0, testDEM.shape))


def test_normals_slope_top_bottom():
    """
    A slope facing north with 45 degree slope should produce a uniform vector field where x and z components are equal and y is zero
    """
    testDEM = np.array([
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50],
        [10, 20,  30, 40, 50]
    ])
    testDEM = np.rot90(testDEM)
    t=surface.study(testDEM, 10)
    assert_allclose(t['SNV'].sel(component="Nx"), np.broadcast_to(0, testDEM.shape))