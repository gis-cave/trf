#
#
import numpy as np
from numpy.testing import assert_allclose
import pytest

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from trf import surface, shader, utils


def test_flat():
    """A flat terrin illuminated from any direction at 45 degrees above horizon will shade at 0.707 (cos(45)) for all cells.
        we are only testing the middle cell, to avoid dealing with edge cases at the peremeter of the DEM. 
    """
    testDEM = np.array([
            [10, 10,  10, 10, 10],
            [10, 10,  10, 10, 10],
            [10, 10,  10, 10, 10],
            [10, 10,  10, 10, 10],
            [10, 10,  10, 10, 10]
        ])
    t = surface.study(testDEM, 10)
    hs = shader.lambert(t, utils.lightVector(270, 45))
    assert hs[2, 2] == pytest.approx(0.707107, 0.0001)
    


def test_inclined():
    """a terrain inclined at 45 will be fully illuminated by a light vector from the west at 45. 
    we are only testing the middle cell, to avoid dealing with edge cases at the peremeter of the DEM. 
    """
    testDEM = np.array([
            [10, 20,  30, 40, 50],
            [10, 20,  30, 40, 50],
            [10, 20,  30, 40, 50],
            [10, 20,  30, 40, 50],
            [10, 20,  30, 40, 50]
        ])
    t = surface.study(testDEM, 10)
    hs = shader.lambert(t, utils.lightVector(270, 0))
    assert hs[2, 2] == pytest.approx(0.707107, 0.0001)

