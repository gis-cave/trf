# testing the tests.
#
#
import numpy as np
from numpy.testing import assert_allclose

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from utils import lightVector, clamp

def test_lightVector():
    testlist = [
        [  0,   0, [0, 1, 0]],
        [  90,  0, [1, 0, 0]],
        [ 270,  0, [-1, 0, 0]],
        [ 180,  0, [0, -1, 0]],
        [   0, 90, [0, 0, 1]],
        [315,  45, [-0.5, 0.5, 0.707107]]
    ]
    for (elev, azimuth, expected) in testlist:
        returnval = np.array(expected)
        lv = lightVector(elev, azimuth)
        assert_allclose(lv, returnval, atol=1e-4)


def test_clamp():
    A = np.array([1.1, 1.0, 0, -1, -1.1, 50])
    c = clamp(A)
    assert_allclose(c, np.array([1, 1, 0, -1, -1, 1]))