#
#

import numpy as np
from numpy.testing import assert_allclose

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

from trf import generalize

testDEM = np.array([
        [0, 0,  0, 0, 0],
        [0, 0,  0, 0, 0],
        [0, 0,  25, 0, 0],
        [0, 0,  0, 0, 0],
        [0, 0,  0, 0, 0],
    ])

def test_Box_Smooth():
    """
    """
    out = generalize.fft_smooth(testDEM, 5, "Box")
    assert_allclose(out, np.ones(out.shape), atol=1e-4)

