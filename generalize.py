# -*- coding: utf-8 -*-
#
# License: GNU GPL v3 -- See 'LICENCE' file.
#
#
import numpy as np
import logging

_logger = logging.getLogger(__name__)
from scipy import ndimage


def fft_smooth(A, s, shape="Box"):
    """
    Applies a low-pass filter across a raster.  Implemented using fast fourier transforms found within the scipy package

    : param A: array-like
    : param s: size of filter (in pixels)
    : param shape: one of (Box, Circle, Gaussian)
    : return: a numpy array, smoothed using the specified filter 
    """
    if shape not in ['Circle', 'Gaussian']:
        shape = 'Box'
    _logger.debug("FFT Smooth: shape={}, size={}".format(shape, s))
    input = np.fft.fft2(A)
    if shape == 'Box':
        result = ndimage.fourier_uniform(input, size=s)
    if shape == 'Circle':
        result = ndimage.fourier_ellipsoid(input, size=s)
    if shape == 'Gaussian':
        result = ndimage.fourier_gaussian(input, sigma=s)

    return np.fft.ifft2(result).real  # only real part; imaginary part of complex number is ignored.


def rof_denoise(img, tolerance=0.1, tau=0.125, tv_weight=100):
    """
    :param img: input image to de-noise
    :param tolerance: when error falls below this number, we're done.
    :param tau: 
    :param tv_weight: 
    :return: denoised and detextured image, texture residual.
    An implementation of the Rudin-Osher-Fatemi (ROF) denoising model
    using the numerical procedure presented in Eq. (11) of A. Chambolle (2005).
    """

    m, n = img.shape  # size of noisy image

    # initialize
    U = img
    Px = np.zeros(img.shape)  # x-component to the dual field
    Py = np.zeros(img.shape)  # y-component of the dual field
    error = 1

    while (error > tolerance):
        Uold = U

        # gradient of primal variable
        grad_Ux = np.roll(U, -1, axis=1) - U  # x-component of U's gradient
        grad_Uy = np.roll(U, -1, axis=0) - U  # y-component of U's gradient

        # update the dual varible
        Px_new = Px + (tau / tv_weight) * grad_Ux  # non-normalized update of x-component (dual)
        Py_new = Py + (tau / tv_weight) * grad_Uy  # non-normalized update of y-component (dual)
        Norm_new = np.maximum(1, np.sqrt(Px_new ** 2 + Py_new ** 2))

        Px = Px_new / Norm_new  # update of x-component (dual)
        Py = Py_new / Norm_new  # update of y-component (dual)

        # update the primal variable
        RxPx = np.roll(Px, 1, axis=1)  # right x-translation of x-component
        RyPy = np.roll(Py, 1, axis=0)  # right y-translation of y-component

        Divergence_P = (Px - RxPx) + (Py - RyPy)  # divergence of the dual field.
        U = img + tv_weight * Divergence_P  # update of the primal variable

        # update of error
        error = np.linalg.norm(U - Uold) / np.sqrt(n * m)
    return U, img - U  # denoised image and texture residual

