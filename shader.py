# -*- coding: utf-8 -*-
#
# License: GNU GPL v3 -- See 'LICENCE' file.
#
#

import numpy as np
import logging
_logger = logging.getLogger(__name__)
from . import utils
from math import sqrt

def lambert(t, lv):
    """
    :param t: terrain xarray dataset.  must be constructed using surface.study such tht it contains data arrays DEM and SNV
    :param lv: 3D light vector as an np.array [x,y,z].  Assumed to be normalized to length one. 
    :return: single band, -1 to 1, per Lambert Cosine Emission Law 

    If you want the shade layer to be in the terrain dataset as a separate dataarray, call this function like so: 
      > terrain = surface.study(dem_array, 7.5)  # numpy array and a cellwidth
      > terrain['hillshade'] = xr.DataArray(data=surface.lambert(terrain), dims=["rows", "cols"])
      > terrain.info
        <bound method Dataset.info of <xarray.Dataset>
        Dimensions:    (rows: 267, cols: 267, component: 3)
        Coordinates:
        * component  (component) <U2 'Nx' 'Ny' 'Nz'
        Dimensions without coordinates: rows, cols
        Data variables:
            DEM        (rows, cols) float32 
            SNV        (rows, cols, component) float32 
            hillshade  (rows, cols) float32
        Attributes:
            cellwidth:  7.5>
    
      You can then access the shade values as terrain['hillshade'] later.
    """
    _logger.debug("Lambertian shader...")
    return np.dot(t['SNV'], lv)



def lommel_seeliger(t, lv):
    """
    :param t: terrain xarray dataset
    :param lv: light vector.
    :return: single band
    """
    cv = np.array([0, 0, 1.0])    ## (camera vector)
    cos_e = np.dot(t['SNV'], cv)
    cos_i = np.dot(t['SNV'], lv)
    BV = np.zeros(cos_i.shape)
    BV[cos_i >= 0] = 1 / (1 + (cos_e[cos_i >= 0] / cos_i[cos_i >= 0]))
    return BV



def oren_nayer(t, lv, sigma):
    """
    :param t: terrain
    :param lightDir:3D vector of light direction
    :param sigma: variation of the roughness distribution
    :return: a single-band, 2D array of brightness values.
    """
    # This code adapted from the implementation at
    # https://github.com/ranjak/opengl-tutorial/blob/master/shaders/illumination/diramb_orennayar_pcn.vert

    # we assume for GIS/cartography that the view direction is overhead.
    cv = np.array([0.0, 0.0, 1.0])  # camera vector 

    # float termA = 1.0f - 0.5f * sigma2 / (sigma2 + 0.57f);
    A = 1 - 0.5 * sigma**2 / (sigma**2 + 0.57)

    # float termB = 0.45f * sigma2 / (sigma2 + 0.09f);
    B = 0.45 * sigma**2 / (sigma**2 + 0.09)

    # Angle from surface to light
    # float cosZi = dot(normCamSpace, dirToLight);
    cosZi = np.dot(t['SNV'], lv)
    ## if <0, light is from 'behind'.  Reflectance is zero.
    cosZi[cosZi<0] = 0

    # Angle from surface to camera
    # float cosZr = dot(normCamSpace, viewDir);
    cosZr = np.dot(t['SNV'], cv)

    # float cosAzimuthSinaTanb = (dot(dirToLight, viewDir) - cosZr * cosZi) / max(cosZr, cosZi);
    cosAz_sinA_tanB = (np.dot(lv, cv) - (cosZr * cosZi)) / np.maximum(cosZr, cosZi)

    # vec4 orenNayarColor = diffuseColor * cosZi * (termA + termB * max(0.0f, cosAzimuthSinaTanb)) *lightIntensity;
    BV = cosZi * (A + B * np.maximum(0, cosAz_sinA_tanB))

    return BV


def minnaerts_reflectance(t, k, lv):
    """
    :param t: terrain xarray dataset
    :param lv: light vector.
    :return: single band
    """
    _k = min(abs(k), 1)
    cv = np.array([0, 0, 1.0])    ## (camera vector)
    cos_e = np.dot(t['SNV'], cv)
    cos_i = np.dot(t['SNV'], lv)

    old_settings = np.seterr(invalid='ignore')
    bv = cos_i**_k * cos_e**(_k-1)
    bv[np.isnan(bv)] = 0
    np.seterr(**old_settings)  # reset to default
    return bv


def shadowLine(t, az, el):
    """
    :param t: terrain xarray dataset.  Must include a dataarray named 'DEM' which is the height field. 
    :param az: Azimuth of light source
    :param el: Elevation of light source
    :return: Array, same shape as the dataset, where 0 values are un-shadowed.  Non-zero cells are in shadow.
             The value is the height difference between actual DEM height and the theoretical height which would
             just peek out of shadow.
    """
    _logger.debug("Casting Shadows...")
    d = t['DEM'].data
    cellwidth = t.attrs['cellwidth']

    returnArray = np.zeros(d.shape)
    lightVector = utils.lightVector(az, el)

    # X,Y as geographic coordinates are not the same as X,Y in numpy arrays.
    # switch to row/col names for numpy manipulations to save our sanity.
    lineMax = returnArray.shape[0] - 1
    colMax = returnArray.shape[1] - 1

    delta_col = lightVector[0]
    delta_row = -lightVector[1]
    deltha_ht = lightVector[2]

    shadowLineArray = np.pad(d, 1, 'edge')

    # The counters increment or decrement based on the dominant light direction.
    # I am not fiddling with the math to get those iterables .... Using if statements to run an unrolled
    # bit of code depending on dominant lighting direction.  This is ugly, but works.

    if (az <= 45.0 or az > 315.0):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(1, lineMax):
            dem = d[line - 1, :]
            n = shadowLineArray[line - 1, 1:-1]
            sl = shadowLineArray[line, 1:-1]
            a = returnArray[line - 1]
            if delta_col <= 0:
                nw = shadowLineArray[line - 1, :-2]
            else:
                nw = shadowLineArray[line - 1, 2:]
            tstHeights = (n * (1 - p)) + (nw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (225 < az <= 315):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(1, colMax):
            dem = d[:, col - 1]
            w = shadowLineArray[1:-1, col - 1]
            sl = shadowLineArray[1:-1, col]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                nw = shadowLineArray[:-2, col - 1]
            else:
                nw = shadowLineArray[2:, col - 1]
            tstHeights = (w * (1 - p)) + (nw * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (135 < az <= 225):
        dH_drow = abs(deltha_ht / delta_row) * cellwidth
        p = abs(delta_col / delta_row)
        for line in range(lineMax, 0, -1):
            dem = d[line, :]
            s = shadowLineArray[line + 2, 1:-1]
            sl = shadowLineArray[line + 1, 1:-1]
            a = returnArray[line]
            if delta_col <= 0:
                sw = shadowLineArray[line + 2, :-2]
            else:
                sw = shadowLineArray[line + 2, 2:]
            tstHeights = (s * (1 - p)) + (sw * (p)) - dH_drow
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    if (45 < az <= 135):
        dH_dcol = abs(deltha_ht / delta_col) * cellwidth
        p = abs(delta_row / delta_col)
        for col in range(colMax, 0, -1):
            dem = d[:, col]
            e = shadowLineArray[1:-1, col + 2]
            sl = shadowLineArray[1:-1, col + 1]
            a = returnArray[:, col - 1]
            if delta_row <= 0:
                ne = shadowLineArray[:-2, col + 2]
            else:
                ne = shadowLineArray[2:, col + 2]
            tstHeights = (e * (1 - p)) + (ne * (p)) - dH_dcol
            sl[dem < tstHeights] = tstHeights[dem < tstHeights]
            a[dem < tstHeights] = tstHeights[dem < tstHeights] - dem[dem < tstHeights]

    return returnArray


def clamp(A):
    _logger.debug("CLAMP")
    return np.clip(A, 0, 1)

def soft(A):
    _logger.debug("SOFT")
    return ((A + 1) / 2)

def half(A):
    _logger.debug("HALF")
    return ((A + 1) / 2) ** 2

def aspect_shading(t, l):
    x = t['SNV'].sel(component="Nx").data
    y = t['SNV'].sel(component="Ny").data
    z = t['SNV'].sel(component="Nz").data

    old_settings = np.seterr(all='ignore') # To suppress warnings about dividing by zero... 
    mag = np.sqrt(x**2 + y**2)
    aspect_vectors = np.stack([x/mag, y/mag, np.zeros(t['DEM'].data.shape)], 2)
    mag = sqrt(l[0]**2 + l[1]**2)
    light_vector = np.array([l[0]/mag, l[1]/mag, 0])
    cos_theta = np.dot(aspect_vectors, light_vector)

    np.seterr(**old_settings)  # reset to default
    return soft(cos_theta)
